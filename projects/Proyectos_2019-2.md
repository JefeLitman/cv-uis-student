---
# Proyectos 2019-2. Visión por Computador

## Prof: Fabio Martínez, Ph.D

---

# Lista de Proyectos
1. [TSIGN: reconocimiento de señas](#proy1)
2. [Segmentación de órganos humanos sobre imágenes de resonancia magnética mediante redes convolucionales](#proy2)
3. [Reconocimiento en video con CNN3D](#proy3)
4. [Hands world](#proy4)
5. [Reconocimiento de texto en cédulas](#proy5)
6. [Text extractor](#proy6)
7. [Clasificador de Parkinson por movimientos oculares](#proy7)
8. [Detección de accidentes automovilísticos](#proy8)

---

## TSIGN: reconocimiento de señas <a name="proy1"></a>

**Autores: Sofia Torres, Sebastian Garcia, Yeison Valencia, Daniel Ardila**

<img src="https://raw.githubusercontent.com/sofiat99/TSIGN-cv/master/Banner.jpeg" style="width:700px;">

**Objetivo: Reconocer señas automaticamente sobre un conjunto de videos**  

- Dataset: Conjunto de señas americanas
- Modelo: BoW, RF, Backgroud substraction, CNN, DNN, 


[(code)](https://github.com/sofiat99/TSIGN-cv) [(video)](https://www.youtube.com/watch?v=AGOJ_qRG0Rc) [(+info)](https://www.canva.com/design/DAD2Miim4sA/pYWPy7DXEthvX7kdLFmzrA/view?utm_content=DAD2Miim4sA&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink)

---

## Segmentación de órganos humanos sobre imágenes de resonancia magnética mediante redes convolucionales <a name="proy2"></a>

**Autores: JORGE  MOGOTOCORO, MARIO  VALLEJO, BRYAN  ALVAREZ **



<img src="https://raw.githubusercontent.com/Jamf05/20192-cv-class-project/master/img/banner.jpg" style="width:500px;">

**Objetivo: Segmentar organos sobre resonancias magneticas  usando redes neuronales profundas**  

- Dataset: tomografías computarizadas 3D de 10 mujeres y 10 hombres con tumores hepáticos en el 75% de los casos.
- Modelo: U-net


[(code)](https://github.com/Jamf05/20192-cv-class-project) [(video)](https://github.com/Jamf05/20192-cv-class-project/blob/master/Video.mp4) [(+info)](https://github.com/Jamf05/20192-cv-class-project/blob/master/Segmentaci%C3%B3n%20de%20%C3%B3rganos%20humanos%20sobre%20im%C3%A1genes%20de%20resonancia%20magn%C3%A9tica%20mediante%20redes%20convolucionales.pptx)

---

## Reconocimiento en video con CNN3D <a name="proy3"></a>

**Autores: Edgar Yesid Rangel Pieschacon**

<img src="https://raw.githubusercontent.com/JefeLitman/CV2019-2_Project/master/CV_banner.png" style="width:700px;">

**Objetivo: Proponer un modelo de aprendizaje profundo convolucional volumetrico que clasifique acciones.**  

- Dataset: UT, JHMDB
- Modelo: convoluciones en 3D


[(code)](https://github.com/JefeLitman/CV2019-2_Project) [(video)](https://drive.google.com/file/d/1iCdJuqrwivIaD8wUtsxcEV07yJtPkYhN/view) [(+info)](https://github.com/JefeLitman/CV2019-2_Project/blob/master/Proyecto_CV.pdf)

---

## Hands world <a name="proy4"></a>

**Autores: Paola Andrea Caicedo Gualdrón, Jhoan Manuel Diaz Higuera, Juan Felipe Peña Herrera**

<img src="https://gitlab.com/paolacaicedouis/hands_words-proyecto_vpc/-/raw/master/imgs/banner.png" style="width:700px;">

**Objetivo:  Identificar gestos realizados con la mano en tiempo real con el fin de interactuar y controlar un sistema informático sin contacto físico directo.**  

- Dataset: 16 clases, 3746 imagenes por clase
- Modelo: CNN   


[(code)](https://gitlab.com/paolacaicedouis/hands_words-proyecto_vpc) [(video)](https://gitlab.com/paolacaicedouis/hands_words-proyecto_vpc/-/blob/master/hands_world-video.mp4) [(+info)](https://gitlab.com/paolacaicedouis/hands_words-proyecto_vpc/-/blob/master/Exposicion-Hands_World-PresentacionInicial.pdf)


---

## Reconocimiento de texto en cédulas <a name="proy5"></a>

**Autores: Juan Ricardo A, Jefferson Murallas, Diego Poveda**

<img src="https://raw.githubusercontent.com/juanrtato/cv_project/master/banner.jpg" style="width:700px;">

**Objetivo: Reconocer el ID de la cédula para agilizar tramines**  

- Dataset: imagenes cédulas propietario
- Modelo: CNN


[(code)](https://github.com/juanrtato/cv_project) [(video)](https://www.youtube.com/watch?v=7__oQCS9cZU&feature=youtu.be) [(+info)](https://github.com/juanrtato/cv_project/blob/master/PROYECTO%20CV.pdf)


---


## Text extractor <a name="proy6"></a>

**Autores: Anngy Nathalia Gomez Avila, Javier Chacón**

<img src="https://github.com/nathaliag30/TEXT-EXTRACTION/blob/master/TEXT%20EXTRACTION.png?raw=true" style="width:700px;">

**Objetivo: Digitalizar información contenida en imagenes que contien texto**  

- Dataset: nist (62 clases, 700000)
- Modelo: CNN + *Bacteria foraing optimization* para segmentación


[(code)](https://github.com/nathaliag30/TEXT-EXTRACTION) [(video)](https://github.com/nathaliag30/TEXT-EXTRACTION/blob/master/VIDEO.pdf) [(+info)](https://github.com/nathaliag30/TEXT-EXTRACTION/blob/master/text%20extractor.pdf)


---

## Clasificador de Parkinson por movimientos oculares <a name="proy7"></a>

**Autores: Victor Mantilla**

<img src="https://github.com/vmantillav/CV_project/blob/master/Banner.png?raw=true" style="width:400px;">

**Objetivo: Desarrollar un clasificador de Parkinson utilizando patrones de movimiento ocular**  

- Dataset: 56 videos
- Modelo: CNN 3D


[(code)](https://github.com/vmantillav/CV_project) [(video)](https://github.com/vmantillav/CV_project/blob/master/Video.mp4) [(+info)](https://github.com/vmantillav/CV_project/blob/master/Presentation.pdf)


---

## Detección de accidentes automovilísticos <a name="proy8"></a>

**Autores: Juan Mantilla**

<img src="https://github.com/juandmantilla/Deteccion-de-accidentes-automovilisticos/blob/master/Notebooks/banner%20Vision2-01.png?raw=true" style="width:400px;">

**Objetivo: Detectar de accidentes automovilísticos por medio de videos de CCTV reales y generados por el software de simulación BeamNG.**  

- Dataset: videos de CCTV reales y generados por el software de simulación BeamNG
- Modelo:  VGG16, MobileNet, Resnet 101 V2, Inception y XCeption.


[(code)](https://github.com/juandmantilla/Deteccion-de-accidentes-automovilisticos) [(video)](https://youtu.be/kZnoI2gNoP0) [(+info)](https://github.com/juandmantilla/Deteccion-de-accidentes-automovilisticos/blob/master/Presentacion.pdf)


---